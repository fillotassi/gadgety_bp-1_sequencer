#!/bin/bash

prefix="dist"
flist="$(find $prefix -type f -name '*.js' -or -name 'index.html' -or -name 'favicon.ico' | sed -e 's/'$prefix'\/*//')"

get_var_name()
{
  echo -e "static_$(echo -e $1 | tr -d '_' | tr '/.' '_')"
}

cat <<EOF
#include "gadgety.h"
#include <ESP8266WebServer.h>

EOF

for i in $flist; do
  echo -e "unsigned char PROGMEM $(get_var_name $i)[] = {"
  gzip -c "$prefix/$i" | xxd -i -c 16
  echo -e "};\n"
done

cat <<EOF
static ESP8266WebServer *_server = 0;

#define SERVE_STATIC(p) static void serve_ ## p (void) { \\
  _server->sendHeader("Content-Encoding", "gzip"); \\
  _server->send_P(200, "text/html", (const char*)p, sizeof(p)); \\
}

static void redirect_index(void)
{
  _server->sendHeader("Location", "/");
  _server->send(301);
}

EOF

for i in $flist; do
  echo -e "SERVE_STATIC($(get_var_name $i))"
done

echo -e "\nvoid gadgety_static_init_server(ESP8266WebServer* server)\n{"
echo -e "  _server = server;"
echo -e "  server->onNotFound(redirect_index);"
echo -e "  server->on(\"/\", serve_static_index_html);"

for i in $flist; do
  echo -e "  server->on(\"/$i\", serve_$(get_var_name $i));"
done

echo -e "}"
